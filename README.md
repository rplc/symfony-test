# Symfony Playground

## How to setup to use in XAMPP
- Host Datei: `127.0.0.1	symfony.strehleralap2.fis-gmbh.de`
- httpd-vhost.conf (`C:\xampp\apache\conf\extra\httpd-vhost.conf`)
```
<VirtualHost *:80>
    DocumentRoot "C:\xampp\htdocs\playground\web\app_dev.php"
    ServerName symfony.strehleralap2.fis-gmbh.de
    ServerAlias www.symfony.strehleralap2.fis-gmbh.de
</VirtualHost>
```

Unter normalen Bedingungen:
- Host Datei: `127.0.0.1 symfony.local`
- httpd-vhost.conf
```
<VirtualHost *:80>
    DocumentRoot "C:\xampp\htdocs\symfony\web\app_dev.php"
    ServerName symfony.local
    ServerAlias www.symfony.local
</VirtualHost>
```